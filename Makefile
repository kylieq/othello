all: reversi.exe
  
reversi.exe: reversi.o
	gcc -o reversi.exe reversi.o

reversi.o: reversi.c
	gcc -c reversi.c

clean:
	rm reversi.o reversi.exe
