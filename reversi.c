#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "reversi.h"

enum disk{black='*', white='o'};

// Initialize the othello data structure.
void initOthello(OthelloType *othello) {
    othello->size = 0;

    // 0 - display player1's disk '*' 
    // 1 - display player2's disk 'o'
    othello->which_disk = 0;

    othello->total_p1_disks = 2;
    othello->total_p2_disks = 2;

    enum disk b = black;
    enum disk w = white;
    othello->player1 = b;
    othello->player2 = w;

    othello->p1Pos = -1;
    othello->p2Pos = -1;

    othello->num_occupied = 0;
    othello->occupied = (int*)malloc((pow(othello->size+1, 2))*sizeof(int));

    othello->empty_slot = '.';
}

// Assign each position of the board an (x,y) coordinate.
void definePos(OthelloType *othello, int **pos_li) {
    int ctr = 0;

    // For very top row (with just column numbers shown), assign
    // each position the coordinate (-1,-1) b/c user will never 
    // select them for disk placement.
    for (int i=0; i<(othello->size+1); i++) {
        for (int j=0; j<2; j++) {
            pos_li[i][j] = -1;
        }
        ctr += 1;
    }

    // Set up list of (row,column) coordinates of board.
    int v_pos = 0;
    int h_pos = 0;
    for (int i=0; i<othello->size; i++) {
        h_pos = 0;
        pos_li[ctr][0] = -1;
        pos_li[ctr][1] = -1;
        ctr += 1;
        int num = 0;
        while (num < othello->size) {
            pos_li[ctr][0] = v_pos;
            pos_li[ctr][1] = h_pos;
            h_pos += 1;
            num += 1;
            ctr += 1;
        }
        v_pos += 1;
    }
}

// Determine where user will place next disk.
void nextMove(OthelloType *othello, int **pos_li, int player) {
    int check1 = requestPosition(othello, pos_li, player);
    int check2 = checkIfValid(othello, check1); 

    // Prompt user for input until it is a valid position for the
    // disk to be placed on board.
    while ((check1 == -1) || (check2 == -1)) {
        printf("Invalid input.\n");
        if (check1 == -1) {
            printf("Position not available.\n\n");
        }
        else if (check2 == -1) {
            printf("Must choose position near a disk that has already been placed.\n\n");
        }
        check1 = requestPosition(othello, pos_li, player);
        check2 = checkIfValid(othello, check1);
    }

    othello->occupied[othello->num_occupied] = check1; 
//    othello->num_occupied += 1;

    if (player == 1) {
        othello->total_p1_disks += 1;
    }
    else {
        othello->total_p2_disks += 1;
    }
}

// Make sure user enters valid input for position on board.
int requestPosition(OthelloType *othello, int **pos_li, int player) {
    int x, y, pos;
    if (player == 1) {
        puts("Player 1 turn, input 2 numbers (0-5) for row and col: ");
    }
    else {
        puts("Player 2 turn, input 2 numbers (0-5) for row and col: ");
    }
    scanf("%d %d", &x, &y);
    printf("\n");

    for (int i=7; i<(pow(othello->size+1, 2)); i++) {

        // If user input (x,y) is in pos_li...
        if ((pos_li[i][0] == x) && (pos_li[i][1] == y)) {

            for (int j=0; j<(pow(othello->size+1, 2)); j++) {

                // if pos of user input (x,y) in pos_li is already in list of occupied points...
                if (i == othello->occupied[j]) {
                    pos = -1;
                    return pos;
                }

                else {
                    pos = i;
                }
            }
        }
    }

    return pos;
}

// Check if user input is near a disk that has already been placed.
int checkIfValid(OthelloType *othello, int pos) {
    for (int i=0; i<pow(othello->size+1, 2); i++) {
        if ((pos == othello->occupied[i]+1) || (pos == othello->occupied[i]-1)){
            return 1;
        }
        else if ((pos == othello->occupied[i] + (othello->size+1)) || (pos == othello->occupied[i] - (othello->size+1))) {
            return 1;
        }
    }
    return -1;
}

// Display board.
void printBoard(OthelloType *othello, int **pos_li, int center1, int center2, int center3, int center4) {
    printf("\n  ");

    // Limit only necessary for board size >=10 (this is just to make the board look nice).
    int limit;
    if (othello->size >= 10) {
        limit = 9;
    }
    else {
        limit = othello->size-1;
    }

    // Display column numbers <10.
    for (int i=0; i<=limit; i++) {
        printf(" %d ", i);
    }

    // Display column numbers >=10 (if applicable).
    for (int i=10; i<othello->size; i++) {
        printf("%d ", i);
    }

    printf("\n");

    int ctr = 0;
    char c;

    int pos = othello->size+1;

    // Display rows with label <10.
    for (int i=0; i<=limit; i++) {
        printf("%d ", ctr);
        pos += 1;
        int j = 0;
        while (j<othello->size) {
            c = determineDisk(othello, pos, center1, center2, center3, center4);
            printf(" %c ", c);
            pos += 1;
            j += 1;
        }
        ctr += 1;
        printf("\n");
    }

    // Display rows with label >=10.
    for (int i=10; i<othello->size; i++) {
        printf("%d", ctr);
        pos += 1;
        int j = 0;
        while (j<othello->size) {
            c = determineDisk(othello, pos, center1, center2, center3, center4);
            printf(" %c ", c);
            pos += 1;
            j += 1;
        }
        ctr += 1;
        printf("\n");
    }

    printf("\n"); 
}

// Determine if either disk or an empty slot will be printed on board.
char determineDisk(OthelloType *othello, int pos, int center1, int center2, int center3, int center4) {
    char c;

    for (int k=0; k<(pow(othello->size+1, 2)); k++) {

        diskColor(othello, k, center1, center2, center3, center4);

        if (othello->occupied[k] == pos) { 
            if (othello->which_disk == 0) {
                c = othello->player1;
                return c;
            }
            else if (othello->which_disk == 1) {
                c = othello->player2;
                return c;
            }
        }
        else {
            c = othello->empty_slot;
        }
    }

    return c;
}

void diskColor(OthelloType *othello, int k, int center1, int center2, int center3, int center4) {
    // If position matches any of the middle positions on board, assign which disk
    // it must hold.
    if ((othello->occupied[k]==center1) || (othello->occupied[k]==center4)) {
        othello->which_disk = 0;
    }
    else if ((othello->occupied[k]==center2) || (othello->occupied[k]==center3)) {
        othello->which_disk = 1;
    }

    // If not center position, then if its index in list occupied is an even number,
    // then it must be player 1's turn. Assign the black (*) disk.
    else if (k%2 == 0) {
        othello->which_disk = 0;
    }

    // If none of the above options, then it must be player 2's turn. Assign the white
    // (o) disk.
    else {
        othello->which_disk = 1;
    }
}

void sizeEven(OthelloType *othello, int **pos_li) {
    int center1 = (pow(othello->size+1, 2))/2;
    int center2 = center1 + 1;
    int center3 = center1 + (othello->size+1);
    int center4 = center3 + 1;

    // Disks on board at beginning of game.
    othello->occupied[0] = center1;
    othello->occupied[1] = center2;
    othello->occupied[2] = center3;
    othello->occupied[3] = center4;
    othello->num_occupied += 4;
        

    int player = -1;

    printBoard(othello, pos_li, center1, center2, center3, center4);
    while (othello->num_occupied < (pow(othello->size, 2))) {
        player = 1;
        nextMove(othello, pos_li, player);
        othello->num_occupied += 1;

        player = 2;
        nextMove(othello, pos_li, player);
        othello->num_occupied += 1;

        printBoard(othello, pos_li, center1, center2, center3, center4);
    }
}

void sizeOdd(OthelloType *othello, int **pos_li) {
    int center1 = ((pow(othello->size+1, 2))/2) + othello->size/2;
    int center2 = center1 + 1;
    int center3 = center1 + (othello->size+1);
    int center4 = center3 + 1;
        
    // Disks on board at beginning of game.
    othello->occupied[0] = center1;
    othello->occupied[1] = center2;
    othello->occupied[2] = center3;
    othello->occupied[3] = center4;
    othello->num_occupied += 4;
       
    int player = -1;
 
    printBoard(othello, pos_li, center1, center2, center3, center4);
    while (othello->num_occupied < (pow(othello->size, 2))) {
        player = 1;
        nextMove(othello, pos_li, player);
        othello->num_occupied += 1;
        
        if (othello->num_occupied >= (pow(othello->size, 2))) {
            break;
        }

        player = 2;
        nextMove(othello, pos_li, player);
        othello->num_occupied += 1;

        printBoard(othello, pos_li, center1, center2, center3, center4);
    } 
}

int main() {
    OthelloType *othello;
    initOthello(othello);

    // Prompt user to enter size for board.
    puts("Please enter the size of the board: "); 
    scanf("%d", &othello->size);

    // Array of all possible positions on board.
    int **pos_li = malloc(sizeof(*pos_li) * (pow(othello->size+1, 2)));
    for (int i=0; i<(pow(othello->size+1, 2)); i++) {
        pos_li[i] = malloc(sizeof(**pos_li));
    }
    definePos(othello, pos_li);

    // Determine whether board size is even or odd.
    if (othello->size%2==0) {
        sizeEven(othello, pos_li);
    }
    else {
        sizeOdd(othello, pos_li);
    }
    
    // Determine result of game.
    if (othello->total_p1_disks == othello->total_p1_disks) {
        printf("It's a tie!\n");
    }
    else if (othello->total_p1_disks > othello->total_p2_disks) {
        printf("Player 1 won!\n");
    }
    else {
        printf("Player 2 won!\n");
    }

    // Free dynamically allocated memory.
    free(othello->occupied);
    free(pos_li);
}

