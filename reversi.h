#ifndef O_H_
#define O_H_

typedef struct {
    // size of board.
    int size;

    // This will hold either 0 (indicating to display player1's disk,
    //  '*') or 1 (indicating to display player2's disk, 'o').
    int which_disk;

    // Total number of disks each player has..
    int total_p1_disks;
    int total_p2_disks;

    // Player1: black disk; Player2: white disk.
    char player1;
    char player2;

    int p1Pos;
    int p2Pos;

    // num_occupied keeps track of the # of positions currently taken.
    int num_occupied;

    // Array of positions taken.
    int *occupied;

    // Character to represent unoccupied position.
    char empty_slot;
} OthelloType;

// Initialize the othello data structure.
void initOthello(OthelloType *othello);

// Assign each position of the board an (x,y) coordinate.
void definePos(OthelloType *othello, int **pos_li);

// Determine where user will place next disk.
void nextMove(OthelloType *othello, int **pos_li, int player);

// Make sure user enters valid input for position on board.
int requestPosition(OthelloType *othello, int **pos_li, int player);

// Check if user input is near a disk that has already been placed.
int checkIfValid(OthelloType *othello, int pos);

// Display board.
void printBoard(OthelloType *othello, int **pos_li, int center1, int center2, int center3, int center4);

// Determine if either disk or an empty slot will be printed on board.
char determineDisk(OthelloType *othello, int pos, int center1, int center2, int center3, int center4);

// Determine which player's disk will be printed next.
void diskColor(OthelloType *othello, int k, int center1, int center2, int center3, int center4);

// Call if board size is even.
void sizeEven(OthelloType *othello, int **pos_li); 

// Call if board size is odd.
void sizeOdd(OthelloType *othello, int **pos_li); 

#endif /* O_H_ */
